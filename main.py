import argparse
import random
from time import sleep
import sys

def main(args):
    forbidden_partners = parse_names(args.names)

    matched_names = randomize_partner(forbidden_partners, seed=random.randint(0,9))
    for name, partner in matched_names.items():
        print("{} gives presents to {}".format(name, partner))

def parse_names(namestring):
    namegroup = namestring.split(";")
    forbidden_partners = {}
    for string in namegroup:
        tmp = string.split(":")
        name = tmp[0]
        if len(tmp)>1:
            forbidden_partners[name]=[name] + tmp[1].split(",")
        else:
            forbidden_partners[name] = [name]
    for name, partners in forbidden_partners.items():
        print("{} never gives to {}".format(name, partners))
    return forbidden_partners

def randomize_partner(forbidden_partners, seed):
    random.seed(seed)
    matched_names = {}
    remaining_partners = list(forbidden_partners.keys()).copy()
    for name in forbidden_partners.keys():
        eligable_partners = remaining_partners.copy()
        for partner in forbidden_partners[name]:
            if partner in eligable_partners:
                eligable_partners.remove(partner)
        if not eligable_partners:
            print("Restarting with new seed...")
            return randomize_partner(forbidden_partners, seed+1) #Restart script with a new seed
        partner = random.sample(eligable_partners, 1)[0]
        remaining_partners.remove(partner)
        matched_names[name] = partner
    return matched_names

if __name__=="__main__":
    parser = argparse.ArgumentParser(description="Code to generate partners for secret santa.")
    parser.add_argument('-n', '--names', help=("List of names to randomize, format:" 
                        "<name1>:<forbidden_partner1>,<forbidden_partner2>;<name2>;<nameN>:"
                        "<forbidden_partner>."),
        type=str, required=True)
    args = parser.parse_args()
    main(args)