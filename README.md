# Secret Santa Randomizer

Code to generate partners for Secret Santa. 

# Prepare the enviroment

1. Clone into the directory, and move into the folder.
2. Create an anaconda enviroment:
    1. `conda create --name secretsanta python=3.8`
    2. `conda activate secretsanta`

# Usage

`python main.py --names <listofnames>`

`<listofnames>` is a string containing the names of each participant.
1. Participants are seperated by ";".
2. If a participant has "forbidden" partners, add a ":" between the participant and the forbidden partners.
3. Several forbidden partners are seperated by ",".

## Example 1 
Imagine Anna, Per, Johan and Julia will play secret santa.

Anna and Per are partners so they don't want to get eachother.
Johan has just given Per and Julia presents so he does not want to get them.

So 
Anna, cannot give to Per (anna:per).
Per, cannot give to Anna (per:anna)
Johan, cannot give to Per or Julia (johan:per,julia)
Julia, can give to all (julia)

Then concatenate by ';' = anna:per;per:anna;johan:per,julia;jula

Run the command:
`python main.py --names anna:per;per:anna;johan:per,julia;jula`